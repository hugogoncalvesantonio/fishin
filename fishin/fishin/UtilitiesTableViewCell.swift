//
//  UtilitiesTableViewCell.swift
//  fishin
//
//  Created by bernardo brito on 18/11/2016.
//  Copyright © 2016 Hugo António. All rights reserved.
//

import UIKit

class UtilitiesTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var utilityImage: UIImageView!
   
    func loadUtility(utility: Utility){
        titleLabel.text = utility.title
        utilityImage.image = utility.image
        self.backgroundColor = utility.color
        titleLabel.textColor = UIColor.white
    }

}
