//
//  TabBarController.swift
//  fishin
//
//  Created by Hugo António on 14/11/2016.
//  Copyright © 2016 Hugo António. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {

    var freshLaunch = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if freshLaunch == true {
            freshLaunch = false
            self.selectedIndex = 2 // 3rd tab
        }

    }
    
}
