//
//  EncyclopediaTableViewCell.swift
//  fishin
//
//  Created by bernardo brito on 21/11/2016.
//  Copyright © 2016 Hugo António. All rights reserved.
//

import UIKit

class EncyclopediaTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var encyImage: UIImageView!
    
    func loadEncyclopedia(encyclopedia: Encyclopedia){
        titleLabel.text = encyclopedia.title
        encyImage.image = encyclopedia.image
        self.backgroundColor = encyclopedia.color
        titleLabel.textColor = UIColor.white

    }
}
