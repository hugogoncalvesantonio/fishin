//
//  TutorialsViewController.swift
//  fishin
//
//  Created by Hugo António on 11/11/2016.
//  Copyright © 2016 Hugo António. All rights reserved.
//

import UIKit

class TutorialsViewController: UITableViewController {
    
    var tutorials: [Tutorial] {
        let tutorials = TutorialTypes()
        return tutorials.tutorialTypes()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - UITableViewDataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            performSegue(withIdentifier: "Methods Segue", sender: self)
            break
        case 1:
            performSegue(withIdentifier: "equipSegue", sender: self)
            break
        case 2:
            performSegue(withIdentifier: "Licences Segue", sender: self)
            break
        case 3:
            performSegue(withIdentifier: "Rules Segue", sender: self)
            break
        default:
            print("Error switching")
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return tutorials.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Tutorial Cell", for: indexPath) as! TutorialTableViewCell
        
        let tutorial = tutorials[indexPath.row]
        
        cell.loadTutorial(tutorial: tutorial)
        
        return cell
    }}

