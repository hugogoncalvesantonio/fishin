//
//  TutorialTableViewCell.swift
//  fishin
//
//  Created by bernardo brito on 21/11/2016.
//  Copyright © 2016 Hugo António. All rights reserved.
//

import UIKit

class TutorialTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tutorialImage: UIImageView!
    
    func loadTutorial(tutorial: Tutorial){
        titleLabel.text = tutorial.title
        tutorialImage.image = tutorial.image
        self.backgroundColor = tutorial.color
        titleLabel.textColor = UIColor.white
    }

}
