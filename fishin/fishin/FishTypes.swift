//
//  EncyclopediaTypes.swift
//  fishin
//
//  Created by bernardo brito on 21/11/2016.
//  Copyright © 2016 Hugo António. All rights reserved.
//

import Foundation

class FishTypes{
    
    init(){
    }
    
    func fishTypes() -> [Fish] {
        return getFishes()
    }
    
    func getFishes() -> [Fish] {
        
        var fishes = [Fish]()
        
        fishes.append(Fish(name: "Moray", imageName: "fish_moray", description: "Atividade: É mais ativa durante noite, escondendo-se em buracos durante o dia\n\nTamanho mínimo de captura: 20 cm\n\nPeríodo de pesca: Janeiro a Setembro\n\nMétodo de pesca: Cana e anzol\n\nIsco: Sardinhas, lulas, camarões"))
        fishes.append(Fish(name: "Salmon", imageName: "fish_salmon", description: "Atividade: Mais ativo durante o dia enquanto efetua a migração\n\nTamanho mínimo de captura: 55cm\n\nPeríodo de pesca: 1 de Março a 31 de Julho\n\nMétodo de pesca: À boia ou ao fundo\n\nIsco: Sardinhas, lulas, camarões pequenos e ovas de salmão"))
        fishes.append(Fish(name: "Sardine", imageName: "fish_sardine", description: "Habitat: Costumam habitar na coluna de água em zonas costeiras de 25-100m de profundidade.\n\nAtividade: De dia protegem-se dos predadores em àguas mais profundas mas durante a noite estão em aguas mais superficiais para se alimentarem\n\nTamanho mínimo de captura: 11cm\n\nPeríodo de pesca: 10 Maio –29 Agosto\n\nMétodo de pesca: É pescada com cerco\n\nIsco: minhoca, navalheira, berbigão."))
        fishes.append(Fish(name: "Iberian Nase", imageName: "fish_iberian", description: "Atividade: É mais ativa durante o dia\n\nTamanho mínimo de captura: 8cm\n\nPeríodo de pesca: Todo o ano\n\nMétodo de pesca: À boia\n\nIsco: Minhoca, navalhada, berbigão"))
        
        return fishes
    }
}
