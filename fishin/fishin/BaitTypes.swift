//
//  EncyclopediaTypes.swift
//  fishin
//
//  Created by bernardo brito on 21/11/2016.
//  Copyright © 2016 Hugo António. All rights reserved.
//

import Foundation

class BaitTypes{
    
    init(){
    }
    
    func baitTypes() -> [Bait] {
        return getBaits()
    }
    
    func getBaits() -> [Bait] {
        
        var baits = [Bait]()
        
        baits.append(Bait(name: "Worms (Cocoon)", description: "Descrição: O casulo é um dos grandes iscos para a pesca quer de costa quer de mar, tendo a preferência de todos os pescadores. É um isco universal e provavelmente o mais usado em Portugal. Tem a particularidade de ter uma risca longitudinal que dentro de agua se torna fluorescente tornando-se muito mais visível e atractivo para o peixe. Devido á sua consistência e tamanho também se mantém muito tempo no anzol resistindo á agitação marítima.\n\nConservação: Usualmente de 3 a 5 dias -entre os 10º e 12º Cº. Existem várias formas de conservar o casulo, sendo que a mais usual é a sua conservação dentro dos canudos onde vêm( vivem), embrulhados em jornal, e guardá-los na gaveta dos legumes. Pode-se também descascar, e mantê-los num recipiente sempre com água limpa e oxigenada. Outra forma muito usada, passa por descascar o casulo e dentro de um pequeno saco estanque com água salgada congelá-los. Desta forma estes mantêm as suas propriedades naturais e ficam mais duros.\n\nComo iscar: https://www.youtube.com/watch?v=5NGH-W0-NTk", imageName: "bait_worm"))
        baits.append(Bait(name: "Crab", description: "Descrição: Há diversos espécies de caranguejo para pescar, sendo todos eles um isco de eleição, para espécies de maior porte. Como por exemplo corvinas, robalos e douradas. Há duas espéciesde caranguejo que aconselhamos: o caranguejo de dois cascos e o caranguejo pilado esta mais difícil de encontrar. O pilado é excelente para pescar com ele vivo,iscando-se o anzol pelo olho do caranguejo com a barbela a sair pela casca, mas lateralmente sem o matar, é ótimo porque ele é um excelente nadador e não pára quieto,atraindo também com a sua cor encarniçada o peixe.\n\nConservação: Sua conservação fresco e vivo, só quem vive à beira-mar ou se dirija a casas especializadas na venda do mesmo é que consegue arranjar. Morto o caranguejo, o seu recheio depressa fica negro, putrefacto, não serve para nada.\n\nComo iscar: https://www.youtube.com/watch?v=5ieSYtM3ZmA", imageName: "bait_crab"))
        baits.append(Bait(name: "Pod Razor", description: "Descrição: São moluscos bivalves, que se encontram protegidos por duas conchas articuladas num ponto. É um isco muito bom, para pescar sobretudo ao fundo, tanto de terra como de barco. O seu odor assim como a sua cor e textura tornam este isco muito eficaz (mas o odor forte também é umadesvantagem visto que é muito complicado remove-lo e é desconfortante).Devendo ser utilizado sempre fresco, mas por vezes obtém bons resultados quando congelado.\n\nConservação:De conservação algo trabalhosa, devem-se manter apertados por um elástico, emmolhos,e de preferência sem estarem em contacto com a água que vão libertando. A zona dos legumes no seu frigorifico é a ideal para os guardar.Se tiver o cuidado de os mergulhar em água salgada de vez em quando, facilmente os manterá vivosdurante uma semana ou mais. Atenção ao cheiro forte e bastante persistente que libertam quando começam a não estar muito frescos que vai “contaminar” tudo o que tiver no frigorifico.Se verificar que os lingueirões não vão chegar vivos até ao dia da sua pescaria, é preferível congelá-los e quando os tirar para pescar, salpique-os com umaspedras de sal para os enrijar ficando assim em perfeitas condições para o efeito.\n\nComo iscar: https://www.youtube.com/watch?v=7cLmtLwg4nk", imageName: "bait_pod"))
        
        return baits
    }
}
