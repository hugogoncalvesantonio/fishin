//
//  EncyclopediaTableViewCell.swift
//  fishin
//
//  Created by bernardo brito on 21/11/2016.
//  Copyright © 2016 Hugo António. All rights reserved.
//

import UIKit

class BaitViewCell: UITableViewCell {
    
    @IBOutlet weak var bait_name: UILabel!
    
    func loadBait(bait: Bait){
        
        self.backgroundColor = UIColor.white //fish.color
        
        self.bait_name.text = bait.name
        
        // titleLabel.textColor = UIColor.white
        
    }
}
