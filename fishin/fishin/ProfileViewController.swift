//
//  ProfileViewController.swift
//  fishin
//
//  Created by Hugo António on 11/11/2016.
//  Copyright © 2016 Hugo António. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController{
    
    var fishingSpots: [FishingSpot] {
        let spots = FishingSpots()
        let spotss = spots.getSpots()
        for i in 1...spotss.count{
        return [spotss[i+1]]
        }
        return []
    }
    
    
    @IBOutlet weak var numberSpotsLabel: UILabel!
    @IBOutlet weak var fishSpotsContainer: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        numberSpotsLabel.text = String(fishingSpots.count)
        passSpotsToTable()
        
        
    }
    
    func passSpotsToTable(){
        let tableController = childViewControllers.last as! ProfileTableViewController
        
        tableController.fishingSpots = fishingSpots
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

