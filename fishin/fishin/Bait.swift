//
//  Fish.swift
//  fishin
//
//  Created by Hugo António on 24/11/2016.
//  Copyright © 2016 Hugo António. All rights reserved.
//

import Foundation
import UIKit

class Bait {
    
    var name: String
    var description : String
    var image: UIImage
    
    init(name: String, description: String, imageName: String) {
        self.name = name
        self.description = description
        self.image = UIImage(named: imageName)!
    }
    
}
