//
//  UtilitiesLine.swift
//  fishin
//
//  Created by bernardo brito on 18/11/2016.
//  Copyright © 2016 Hugo António. All rights reserved.
//

import Foundation

class UtilitiesTypes{
    
    init(){
    }
    
    func utilitiesTypes() -> [Utility] {
        return getUtilities()
    }
    
    func getUtilities() -> [Utility] {
        
        var utilities = [Utility]()
        
        utilities.append(Utility(title: "Weather Forecasting", imageName: "util_weather", hex: 0x3DA5D9))
        utilities.append(Utility(title: "Tide cycles", imageName: "util_tide", hex: 0x0089D0))
        utilities.append(Utility(title: "Solar and lunar cycles", imageName: "util_solar", hex: 0xF47920))
        utilities.append(Utility(title: "Fish activity", imageName: "util_fish", hex: 0xFCAF17))
        utilities.append(Utility(title: "Sea surface currents", imageName: "util_sea", hex: 0x2364AA))
        
        return utilities
    }
}
