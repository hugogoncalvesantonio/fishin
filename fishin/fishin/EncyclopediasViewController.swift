//
//  EncyclopediasViewController.swift
//  fishin
//
//  Created by Hugo António on 11/11/2016.
//  Copyright © 2016 Hugo António. All rights reserved.
//

import UIKit

class EncyclopediasViewController: UITableViewController {
    
    var encyclopedias: [Encyclopedia] {
        let encyclopedias = EncyclopediaTypes()
        return encyclopedias.encyclopediaTypes()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - UITableViewDataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            performSegue(withIdentifier: "fishSegue", sender: self)
            break
        case 1:
            performSegue(withIdentifier: "baitSegue", sender: self)
            break
        default:
            print("Error switching")
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return encyclopedias.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Encyclopedia Cell", for: indexPath) as! EncyclopediaTableViewCell
        
        let ency = encyclopedias[indexPath.row]
        
        cell.loadEncyclopedia(encyclopedia: ency)
        
        return cell
    }}


