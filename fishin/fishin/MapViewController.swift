//
//  MapViewController.swift
//  fishin
//
//  Created by Hugo António on 11/11/2016.
//  Copyright © 2016 Hugo António. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class MapViewController: UIViewController, CLLocationManagerDelegate , MKMapViewDelegate{
      
    @IBAction func centerOnUserButton(_ sender: UIButton) {
        centerOnUser = true
        viewDidLoad()
    }
    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var FishesOnSpot: UIView!
    var locationManager: CLLocationManager!
    var uilpgr : UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: Selector(("addSpot:")))
    var centerCoord : CLLocationCoordinate2D?
    var centerOnUser = true
    
    var stores: [Store] {
        let stores = Stores()
        return stores.getStores()
    }
    var fishingSpots: [FishingSpot] {
        let spots = FishingSpots()
        return spots.getSpots()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(CLLocationManager.locationServicesEnabled())
        {
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
            self.map.showsUserLocation = true
        }
        
        self.map.delegate = self
        
        
        
        uilpgr.minimumPressDuration = 2.0
        map.addGestureRecognizer(uilpgr)

    
        addAllSpotsToMap()
        
    }
    
    func addAllSpotsToMap(){
        
        for i in 0...stores.count-1{
            let annotation = StorePointAnnotation()
            annotation.coordinate = stores[i].coordinates
            annotation.title = stores[i].name
            annotation.subtitle = stores[i].description
            map.addAnnotation(annotation)
            
        }
        
        for i in 0...fishingSpots.count-1{
            let annotation = FishPointAnnotation()
            annotation.coordinate = fishingSpots[i].coordinates
            annotation.title = fishingSpots[i].spotName
            annotation.subtitle = fishingSpots[i].description
            annotation.spot = fishingSpots[i]
            map.addAnnotation(annotation)
        }

        

    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if(centerOnUser){
            let location = locations.last
        
            let center = CLLocationCoordinate2D(latitude: location!.coordinate.latitude, longitude: location!.coordinate.longitude)
            let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1))
            self.map.setRegion(region, animated: true)
        
            locationManager.stopUpdatingLocation()
        }else{
            let center = centerCoord
            let region = MKCoordinateRegion(center: center!, span: MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1))
            self.map.setRegion(region, animated: true)
            //centerOnUser = true
        }
        
        
        
        
    }
    
    func mapView(_ map: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        // Don't want to show a custom image if the annotation is the user's location.
        guard !(annotation is MKUserLocation) else {
            return nil
        }
        
        // Better to make this class property
        let annotationIdentifier = "AnnotationIdentifier"
        
        var annotationView: MKAnnotationView?
        if let dequeuedAnnotationView = map.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier) {
            annotationView = dequeuedAnnotationView
            annotationView?.annotation = annotation
        }
        else {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            annotationView?.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        }
        
        if let annotationView = annotationView {
            // Configure your annotation view here
            annotationView.canShowCallout = true
            if annotation is FishPointAnnotation{
                annotationView.image = UIImage(named: "map_fish")
            }else{
                annotationView.image = UIImage(named: "map_store")
            }
           
        }
        
        return annotationView
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .default
    }

    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl){
        
        if (control == view.rightCalloutAccessoryView)
        {
            if (view.annotation is FishPointAnnotation){
                performSegue(withIdentifier: "FishSpot", sender: view.annotation)
            }/*else{
                performSegue(withIdentifier: "Store", sender: view.annotation)
            }*/
        }
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "FishSpot"){
            let spotControl = segue.destination as! FishingSpotViewController
            
            spotControl.spot = (sender as! FishPointAnnotation).spot
            //spotControl. = ((sender as! MKAnnotationView).annotation?.title)!
            //spotControl.spotName.text = ((sender as! MKAnnotationView).annotation?.title)!
            //spotControl.spotName.text = ((sender as! MKAnnotationView).annotation?.title)!
        }//else if(segue.identifier == "Store"){
            
        //}
    }
    @IBAction func addFishingSpot(_ sender: UILongPressGestureRecognizer) {
        if(sender.state == UIGestureRecognizerState.began)
        {
            print("gesture")
            //locationInView = Returns the point computed as the location in a given view of the gesture represented by the receiver.
            let touchPoint = sender.location(in: self.map)
            
            //convertPoint = convert a point from map to coordinate
            let newCoordinate = self.map.convert(touchPoint, toCoordinateFrom: self.map)
            
            let annotation = MKPointAnnotation()
            
            performSegue(withIdentifier: "addSpotSegue", sender: nil)
            annotation.coordinate = newCoordinate
            annotation.title = "New Annotation"
            self.map.addAnnotation(annotation)
            print("gesture")
        }
            
    }


}
