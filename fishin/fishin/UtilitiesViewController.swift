//
//  UtilitiesViewController.swift
//  fishin
//
//  Created by bernardo brito on 17/11/2016.
//  Copyright © 2016 Hugo António. All rights reserved.
//

import UIKit

class UtilitiesViewController: UITableViewController
{
    
    var utilities: [Utility] {
        let utilities = UtilitiesTypes()
        return utilities.utilitiesTypes()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    // MARK: - UITableViewDataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
    return utilities.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Utility Cell", for: indexPath) as! UtilitiesTableViewCell
        
        let util = utilities[indexPath.row]
        
        cell.loadUtility(utility: util)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            performSegue(withIdentifier: "weather segue", sender: self)
            break
        case 1:
            performSegue(withIdentifier: "tides segue", sender: self)
            break
        case 2:
            performSegue(withIdentifier: "cycles segue", sender: self)
            break

        case 3:
            performSegue(withIdentifier: "activity segue", sender: self)
            break

        case 4:
            performSegue(withIdentifier: "currents segue", sender: self)
            break
        default:
            break
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
