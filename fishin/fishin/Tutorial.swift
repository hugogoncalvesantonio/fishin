//
//  Tutorial.swift
//  fishin
//
//  Created by bernardo brito on 21/11/2016.
//  Copyright © 2016 Hugo António. All rights reserved.
//

import Foundation
import UIKit

class Tutorial {
    
    var title: String
    var image: UIImage
    var color: UIColor = UIColor.white
    
    init(title: String, imageName: String, hex: Int) {
        self.title = title
        self.image = UIImage(named: imageName)!
        self.color = getColor(hex: hex)
    }
    
    func getColor(hex: Int) -> UIColor{
        return UIColor(
            red:(CGFloat)((hex & 0xFF0000) >> 16)/255.0,
            green:((CGFloat)((hex & 0x00FF00) >>  8))/255.0 ,
            blue:((CGFloat)((hex & 0x0000FF) >>  0))/255.0 ,
            alpha:1.0)
    }
    

}
