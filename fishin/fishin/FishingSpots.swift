//
//  Strores.swift
//  fishin
//
//  Created by bernardo brito on 25/11/2016.
//  Copyright © 2016 Hugo António. All rights reserved.
//

import Foundation
import MapKit
import CoreLocation

class FishingSpots{
    
    init(){
    }
    
    func fishingSpots() -> [FishingSpot] {
        return getSpots()
    }
    
    func getSpots() -> [FishingSpot] {
        
        var spots = [FishingSpot]()
        
        spots.append(FishingSpot(name: "Trafaria", description: "Lorem ipsum", coord: CLLocationCoordinate2D(
            latitude: Double(38.6691),
            longitude: Double(-9.2457)
        ),fishes: [Fish(name: "Pargo", imageName: "fish_salmon", description: ""),Fish(name: "Dourada", imageName: "fish_salmon", description: ""),Fish(name: "bacalhau", imageName: "fish_salmon", description: ""),Fish(name: "sargo", imageName: "fish_salmon", description: "")]))
        spots.append(FishingSpot(name: "Costa da Caparica", description: "Lorem ipsum", coord: CLLocationCoordinate2D(
            latitude: Double(38.6283),
            longitude: Double(-9.2167)
        ),fishes: [Fish(name: "Robalo", imageName: "fish_salmon", description: ""),Fish(name: "Sardinha", imageName: "fish_salmon", description: "")]))
        
        spots.append(FishingSpot(name: "Pescaria", description: "Lorem ipsum", coord: CLLocationCoordinate2D(
            latitude: Double(38.6925),
            longitude: Double(-9.2239)
        ),fishes: [Fish(name: "Cavala", imageName: "fish_salmon", description: ""),Fish(name: "Carapau", imageName: "fish_salmon", description: "")]))
        
        spots.append(FishingSpot(name: "Doca Pesca", description: "Lorem ipsum", coord: CLLocationCoordinate2D(
            latitude: Double(38.6954),
            longitude: Double(-9.2290)
        ),fishes: [Fish(name: "Robalo", imageName: "fish_salmon", description: ""),Fish(name: "Dourada", imageName: "fish_salmon", description: "")]))
        
        spots.append(FishingSpot(name: "Cacilhas", description: "Lorem ipsum", coord: CLLocationCoordinate2D(
            latitude: Double(38.6882),
            longitude: Double(-9.1486)
        ),fishes: [Fish(name: "Cavala", imageName: "fish_salmon", description: ""),Fish(name: "Enguia", imageName: "fish_salmon", description: "")]))
        
        return spots
    }
}
