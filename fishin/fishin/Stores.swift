//
//  Strores.swift
//  fishin
//
//  Created by bernardo brito on 25/11/2016.
//  Copyright © 2016 Hugo António. All rights reserved.
//

import Foundation
import MapKit
import CoreLocation

class Stores{
    
    init(){
    }
    
    func storeTypes() -> [Store] {
        return getStores()
    }
    
    func getStores() -> [Store] {
        
        var store = [Store]()
        
        store.append(Store(name: "pescaAqui", description: "Lorem ipsum", coord: CLLocationCoordinate2D(
            latitude: Double(38.6786),
            longitude: Double(-9.1583)
            )))
        store.append(Store(name: "FishIt", description: "Lorem ipsum", coord: CLLocationCoordinate2D(
            latitude: Double(38.6283),
            longitude: Double(-9.2167)
        )))

        store.append(Store(name: "PescariaLda", description: "Lorem ipsum", coord: CLLocationCoordinate2D(
            latitude: Double(38.6992),
            longitude: Double(-9.2239)
        )))

        store.append(Store(name: "Onda&Pesca", description: "Lorem ipsum", coord: CLLocationCoordinate2D(
            latitude: Double(38.6912),
            longitude: Double(-9.3233)
            )))
        
        return store
    }
}
