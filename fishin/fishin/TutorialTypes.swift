//
//  TutorialTypes.swift
//  fishin
//
//  Created by bernardo brito on 21/11/2016.
//  Copyright © 2016 Hugo António. All rights reserved.
//

import Foundation

class TutorialTypes{
    
    init(){
    }
    
    func tutorialTypes() -> [Tutorial] {
        return getTutorials()
    }
    
    func getTutorials() -> [Tutorial] {
        
        var tutorials = [Tutorial]()
        
        tutorials.append(Tutorial(title: "Fishing methods", imageName: "tutorials_method", hex: 0x3DA5D9))
        tutorials.append(Tutorial(title: "Equipment", imageName: "tutorials_equipment", hex: 0x0089D0))
        tutorials.append(Tutorial(title: "Licences", imageName: "tutorials_licenses", hex: 0xF47920))
        tutorials.append(Tutorial(title: "Rules and legislation", imageName: "tutorials_rules", hex: 0xFCAF17))
        
        return tutorials
    }
}
