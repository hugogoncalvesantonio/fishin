//
//  EncyclopediasViewController.swift
//  fishin
//
//  Created by Hugo António on 11/11/2016.
//  Copyright © 2016 Hugo António. All rights reserved.
//

import UIKit

class BaitDetailViewController: UIViewController {
    
    
    @IBOutlet weak var bait_image: UIImageView!

    @IBOutlet weak var bait_name: UILabel!
    
    @IBOutlet weak var bait_desc: UITextView!
    
    var bait : Bait!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = bait.name
        
        self.bait_name.text = bait.name
        self.bait_image.image = bait.image
        self.bait_desc.text = bait.description
    }
    
}

