//
//  Fish.swift
//  fishin
//
//  Created by Hugo António on 24/11/2016.
//  Copyright © 2016 Hugo António. All rights reserved.
//

import Foundation
import UIKit

class Fish {
    
    var name: String
    var image: UIImage
    var description : String
    
    init(name: String, imageName: String, description: String) {
        self.name = name
        self.image = UIImage(named: imageName)!
        self.description = description
    }
    
}
