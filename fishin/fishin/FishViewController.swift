//
//  EncyclopediasViewController.swift
//  fishin
//
//  Created by Hugo António on 11/11/2016.
//  Copyright © 2016 Hugo António. All rights reserved.
//

import UIKit

class FishViewController: UITableViewController {
    
    var selectedFish : Fish!
    
    var fishes: [Fish] {
        let fishes = FishTypes()
        return fishes.fishTypes()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.backgroundColor = getColor(hex: 0x3DA5D9)
        
        navigationItem.title = "Fishes"
        
    }
    
    func getColor(hex: Int) -> UIColor{
        return UIColor(
            red:(CGFloat)((hex & 0xFF0000) >> 16)/255.0,
            green:((CGFloat)((hex & 0x00FF00) >>  8))/255.0 ,
            blue:((CGFloat)((hex & 0x0000FF) >>  0))/255.0 ,
            alpha:1.0)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if(segue.identifier == "fishDetail") {
            
            var path = self.tableView.indexPathForSelectedRow!
            
            let yourNextViewController = segue.destination as! FishDetailViewController
            
            yourNextViewController.fish = fishes[path.row]
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) ->Int{
        return fishes.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Fish View Cell", for: indexPath) as! FishViewCell
        
        let fish = fishes[indexPath.row]
        
        cell.contentView.backgroundColor = getColor(hex: 0x3DA5D9)
        
        cell.loadFish(fish: fish)
        
        return cell
    }

    
    
}

