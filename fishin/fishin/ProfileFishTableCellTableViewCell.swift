//
//  ProfileFishTableCellTableViewCell.swift
//  fishin
//
//  Created by bernardo brito on 25/11/2016.
//  Copyright © 2016 Hugo António. All rights reserved.
//

import UIKit

class ProfileFishTableCellTableViewCell: UITableViewCell {

    var spot: FishingSpot!
    
    
    @IBOutlet weak var marker_point: UIImageView!
    @IBOutlet weak var spot_name: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func loadName(name: String){
        spot_name.text = name
        marker_point.image = UIImage(named: "profile_marker")
    }
}
