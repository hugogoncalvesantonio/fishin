//
//  EncyclopediasViewController.swift
//  fishin
//
//  Created by Hugo António on 11/11/2016.
//  Copyright © 2016 Hugo António. All rights reserved.
//

import UIKit

class FishDetailViewController: UIViewController {
    
    @IBOutlet weak var fish_image: UIImageView!
    
    @IBOutlet weak var fish_name: UILabel!
    
    @IBOutlet weak var fish_desc: UITextView!
    
    var fish : Fish!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = fish.name
        
        self.fish_name.text = fish.name
        self.fish_image.image = fish.image
        self.fish_desc.text = fish.description
    }
    
}

