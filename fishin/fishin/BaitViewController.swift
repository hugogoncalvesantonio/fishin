//
//  EncyclopediasViewController.swift
//  fishin
//
//  Created by Hugo António on 11/11/2016.
//  Copyright © 2016 Hugo António. All rights reserved.
//

import UIKit

class BaitViewController: UITableViewController {
    
    var baits: [Bait] {
        let baits = BaitTypes()
        return baits.baitTypes()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.backgroundColor = getColor(hex: 0x0089D0)

        navigationItem.title = "Baits"        
    }
    
    func getColor(hex: Int) -> UIColor{
        return UIColor(
            red:(CGFloat)((hex & 0xFF0000) >> 16)/255.0,
            green:((CGFloat)((hex & 0x00FF00) >>  8))/255.0 ,
            blue:((CGFloat)((hex & 0x0000FF) >>  0))/255.0 ,
            alpha:1.0)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if(segue.identifier == "baitDetail") {
            
            var path = self.tableView.indexPathForSelectedRow!
            
            let yourNextViewController = segue.destination as! BaitDetailViewController
            
            yourNextViewController.bait = baits[path.row]
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) ->Int{
        return baits.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Bait View Cell", for: indexPath) as! BaitViewCell
        
        let bait = baits[indexPath.row]
        
        cell.contentView.backgroundColor = getColor(hex: 0x0089D0)
        
        cell.loadBait(bait: bait)
        
        return cell
    }}

