//
//  FishingSpot.swift
//  fishin
//
//  Created by bernardo brito on 25/11/2016.
//  Copyright © 2016 Hugo António. All rights reserved.
//

import Foundation
import MapKit
import CoreLocation

class FishingSpot{

    var coordinates = CLLocationCoordinate2D(
        latitude: Double(38.6768),
        longitude: Double(-9.2139)
    )
    var fishes : [Fish] = []
    var spotName : String = ""
    var description : String = ""
    
    init( name: String , description: String ,coord: CLLocationCoordinate2D,fishes: [Fish]){
        self.coordinates = coord
        self.spotName = name
        self.description = description
        self.fishes = fishes
        
    }
    
    
    
    

}
