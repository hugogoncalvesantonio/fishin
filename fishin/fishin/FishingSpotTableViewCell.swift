//
//  FishingSpotTableViewCell.swift
//  fishin
//
//  Created by bernardo brito on 28/11/2016.
//  Copyright © 2016 Hugo António. All rights reserved.
//

import UIKit

class FishingSpotTableViewCell: UITableViewCell {
    
    @IBOutlet weak var fish_name: UILabel!

    func loadFish(fish: Fish){
        
        self.backgroundColor = UIColor.white //fish.color
        
        self.fish_name.text = fish.name
        
        // titleLabel.textColor = UIColor.white
        
    }

}
