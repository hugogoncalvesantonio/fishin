//
//  FishingSpotViewController.swift
//  fishin
//
//  Created by bernardo brito on 25/11/2016.
//  Copyright © 2016 Hugo António. All rights reserved.
//

import UIKit

class FishingSpotViewController: UIViewController {

    var spot : FishingSpot!
    
    @IBOutlet weak var latitude: UILabel!
    @IBOutlet weak var longitude: UILabel!
    @IBOutlet weak var notes: UITextView!
    @IBOutlet weak var addFavorites: UIButton!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var tableContainer: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        latitude.text = String(spot.coordinates.latitude)
        longitude.text = String(spot.coordinates.longitude)
        notes.text = spot.description
        name.text = spot.spotName
        sendFishesToTable()
        // Do any additional setup after loading the view.
    }
    
    func sendFishesToTable(){
        let cont = childViewControllers.last as! FishingSpotTableViewController
        cont.fishes = spot.fishes
    
    }
    
}
