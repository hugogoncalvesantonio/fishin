//
//  EncyclopediaTypes.swift
//  fishin
//
//  Created by bernardo brito on 21/11/2016.
//  Copyright © 2016 Hugo António. All rights reserved.
//

import Foundation

class EncyclopediaTypes{
    
    init(){
    }
    
    func encyclopediaTypes() -> [Encyclopedia] {
        return getEncyclopedias()
    }
    
    func getEncyclopedias() -> [Encyclopedia] {
        
        var encyclopedias = [Encyclopedia]()
        
        encyclopedias.append(Encyclopedia(title: "Fish Encyclopedia", imageName: "ency_fish", hex: 0x3DA5D9))
        encyclopedias.append(Encyclopedia(title: "Bait Encyclopedia", imageName: "ency_bait", hex: 0x0089D0))
        
        return encyclopedias
    }
}
