//
//  FishPointAnnotation.swift
//  fishin
//
//  Created by bernardo brito on 25/11/2016.
//  Copyright © 2016 Hugo António. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class FishPointAnnotation: MKPointAnnotation{
    
    var Image = UIImage()
    var spot = FishingSpot(name: "", description: "",coord: CLLocationCoordinate2D(latitude: 37.3454, longitude: 32.4654),fishes: [])
}
