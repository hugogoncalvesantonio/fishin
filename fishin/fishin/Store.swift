//
//  Store.swift
//  fishin
//
//  Created by bernardo brito on 25/11/2016.
//  Copyright © 2016 Hugo António. All rights reserved.
//
import UIKit
import MapKit
import CoreLocation

class Store{

    var coordinates = CLLocationCoordinate2D(
        latitude: Double(38.6768),
        longitude: Double(-9.2139)
    )
    var name : String = ""
    var description : String = ""
    
    init(name: String, description: String, coord: CLLocationCoordinate2D){
        self.name = name
        self.coordinates = coord
        self.description = description
    }
    
}
