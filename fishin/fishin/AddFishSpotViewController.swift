//
//  AddFishSpotViewController.swift
//  fishin
//
//  Created by bernardo brito on 25/11/2016.
//  Copyright © 2016 Hugo António. All rights reserved.
//

import UIKit

class AddFishSpotViewController: UIViewController {

    @IBOutlet weak var localName: UITextField!
    @IBOutlet weak var fishesCaught: UITableView!
    @IBOutlet var Notes: UIView!
    @IBOutlet weak var publicLocal: UISwitch!
    @IBOutlet weak var cancel: UIButton!
    @IBOutlet weak var submit: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

}
